USE classic_models;

-- 1. Return the customerName of the customers who are from the Philippines.
SELECT customerName FROM customers WHERE country="Philippines";

-- 2. Return the contactLastName and contactFirstName of the customer with the name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName FROM customers WHERE customerName="La Rochelle Gifts";

-- 3. Return the the product name and MSRP of the product named "The Titanic"
SELECT productName, MSRP FROM products WHERE productName="The Titanic";

-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT firstName, lastName FROM employees WHERE email="jfirrelli@classicmodelcars.com";

-- 5 Return the names of the customers who have no registered state
SELECT customerName FROM customers WHERE state IS NULL;

-- 6. Return the first name, last name, email of the employee whoe last name is Patterson and first name is Steve
SELECT firstName, lastName, email FROM employees WHERE lastName="Patterson" AND firstName="Steve";

-- 7. Return customer name, country and credit limit of the customer whose countries are NOT USA and whose credit are greater than 3000.
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8. Return the customer number of order whose comments contain the string 'DHL'
SELECT customerNumber, comments FROM orders WHERE comments LIKE "%DHL%";

-- 9. Return the product lines whose text description mentions the phrase 'state of the art' 
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10. Return the countries of customers without duplication
SELECT DISTINCT country FROM customers;

-- 11. Return the statuses of order without dupliction
SELECT DISTINCT status FROM orders;

-- 12. Return the customer names and countried of customers whose country is USA, France, or Canada.
SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo
SELECT firstName, lastName, offices.city FROM employees
		JOIN offices ON employees.officeCode = offices.officeCode
		WHERE offices.city = 'Tokyo';

-- 14. Return the customer names of customers who were served by the employee names "Leslie Thompson".
SELECT customerName FROM customers
		JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
		WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";

-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"
SELECT products.productName, customers.customerName 
		FROM orders
		JOIN customers ON orders.customerNumber = customers.customerNumber
		JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
		JOIN products ON products.productCode = orderdetails.productCode
		WHERE customers.customerName = "Baane Mini Imports";

-- 16. Return the employees' first names, employees' last names, customers' name, and offices' countries  of transaction whose customers and offices are located in the same country
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country
		FROM orders
		JOIN customers ON orders.customerNumber = customers.customerNumber
		JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
		JOIN offices ON offices.officeCode = employees.officeCode
		WHERE customers.city = offices.city;


-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
SELECT productName, quantityInStock FROM products WHERE productline = "Planes" AND quantityInStock <1000;

-- 18. Return the customer's name with a phone number containing "+81"
SELECT customerName from customers WHERE phone LIKE "%+81%";